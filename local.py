#!flask/bin/python
'''Executable script for local testing

This script can be executed to start the Flask server locally. It will then be
available at http://localhost:5000/. This should not be used for production, as
it is not deemed safe.
'''

import sys
import os
from app import app


class FlaskTestClientProxy(object):
    '''WSGI Proxy that simulates the CERN SSO Proxy

    The purpose of the proxy is the insertion of the username into the HTTP
    header. This name is used when editing samples and would otherwise be None
    when testing locally.
    '''
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        environ['HTTP_X_REMOTE_USER'] = os.environ.get('CERNUSERNAME', 'anonymous')
        return self.app(environ, start_response)


def main():
    # Inject the WSGI proxy for local testing
    app.wsgi_app = FlaskTestClientProxy(app.wsgi_app)

    # Run the flask application
    app.run(host='0.0.0.0', port=8080)


if __name__ == '__main__':
    main()
    sys.exit(0)
