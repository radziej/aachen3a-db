# stdlib imports
from functools import wraps
from flask import request, jsonify
from datatables import ColumnDT, DataTables
from sqlalchemy import inspect
from sqlalchemy.orm import contains_eager
from sqlalchemy.exc import DatabaseError

# local imports
from . import app, session, models, schemas, columns


# Handle invalid API usage
class APIException(Exception):
    def __init__(self, message, status_code=400, payload=None):
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        # build dict from payload (or empty) and attach message
        rv = dict(self.payload or ())
        rv['message'] = self.message
        # return dict
        return rv


@app.errorhandler(APIException)
def handle_apierror(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# Supportive functions
def require_json(func):
    '''Decorator to ensure that the route is called with a JSON payload

    By applying this decorator to routes, the route will throw a 400 error code
    noting that a JSON payload is missing, if it is not provided.
    '''

    @wraps(func)
    def wrapper(*args, **kwargs):
        # Raise an exception if no JSON is provided
        if request.get_json() is None:
            raise APIException('No payload JSON provided')
        # Proceed normally if JSON is provided
        return func(*args, **kwargs)
    return wrapper


def excepts(func):
    '''Catch database API exceptions and forward them to the user

    This decorator is meant to be used for functions where a database API call
    depends on user input. Should this input be malformed and raise an error,
    an `APIException` is raised to notify the user.'''

    @wraps(func)
    def wrapper(*args, **kwargs):
        # Execute the statement
        try:
            return func(*args, **kwargs)
        except DatabaseError as e:
            # If an exception was raised, inform the user via the default
            # exception handling procedure
            raise APIException('Encountered an exception while executing your '
                               'request:\n{}'.format(e.message))
    return wrapper


def get_model(name):
    '''Retrieves the model class

    This function retrieves the model from the models module based on the name
    of the model class. An APIException is raised, if it cannot be found.
    '''

    model = getattr(models, name, None)
    if not model:
        raise APIException('Invalid model: {0}'.format(name))
    return model


def get_schema(name):
    '''Retrieves the model schema instance

    This function retrieves the schema from the schemas module based on the
    name of the model class. An APIException is raised, if it cannot be found.
    '''

    schema = getattr(schemas, 'schema_' + name, None)
    if not schema:
        raise APIException('Invalid schema: {0}'.format('schema_' + name))
    return schema


def query_unique(classname, id=None, name=None):
    '''Performs a query for a unique database entry

    If neither id nor name are provided, an error is raised.
    '''

    # Ensure that either the name or ID is given
    if not name and not id:
        raise APIException('Operation requires an ID or a name')
    # Ensure that id is used for Skims
    if name and 'Skim' in classname:
        raise APIException('Cannot query Skim by name')

    # Retrieve database entry
    model = get_model(classname)
    if id:
        entry = session.query(model).filter_by(id=id).one_or_none()
    else:
        entry = session.query(model).filter_by(name=name).one_or_none()
    if not entry:
        raise APIException('No {} entry found for: {}'.format(classname,
                                                              id or name))

    # Return result
    return entry


def validated_instance(classname):
    '''Create a class instance from classname and request JSON

    Taking the JSON payload from the request, a class instance of `classname `
    is created using the appropriate schema. This implies that all fields are
    validated. An error is raised, if validation fails.
    '''

    # Load JSON into schema (without any errors)
    schema = get_schema(classname)
    entry, errors = schema.load(request.get_json())
    if errors:
        raise APIException('Fields are not configured correctly.',
                           payload=errors)

    # Return entry class instance
    return entry


# Filter set class
class FilterSet(object):
    '''Set of filters which can be applied to a query

    This class applies filters in the form of 3-tuples to queries.
    '''

    def __init__(self, model, filters):
        # try to initialize model
        self.model = model
        self.filters = filters


    def parse_filter(self, entry):
        # ensure that all 3 expected fields are available
        if (not 'field' in entry or
            not 'value' in entry or
            not 'operator' in entry):
            raise APIException('Invalid filter {0}'.format(entry))
        # return 3 fields
        return entry['field'], entry['value'], entry['operator']


    def filter_query(self, query):
        '''Filter query by list of filter tuples

        Implementation heavily inspired by:
        http://stackoverflow.com/questions/14845196/dynamically-constructing-filters-in-sqlalchemy
        '''

        # Loop over all tuples of filters
        for entry in self.filters:
            # get components of filter
            field, value, operator = self.parse_filter(entry)
            # retrieve column
            column = getattr(self.model, field, None)
            if not column:
                raise APIException('Invalid column: {0}'.format(field))

            # Find function matching the provided column operator
            try:
                # Check for each function template if such a function exists
                function = [
                    template for template in ['{0}', '{0}_', '__{0}__']
                    if hasattr(column, template.format(operator))
                ][0].format(operator)
            except IndexError:
                # If there was no matching function raise APIException
                raise APIException('Invalid filter operator: {0}'.format(operator))

            # build filter operation and apply to query
            operation = getattr(column, function)(value)
            query = query.filter(operation)
        # return filtered query
        return query


# AJAX Routes
@app.route('/ajax/<string:classname>')
@excepts
def get_ajax(classname):
    '''Return selected columns for requested Class'''

    # Retrieve model
    model = get_model(classname)
    # Retrieve requested model fields
    args = request.args.to_dict()
    fields = []
    i = 0
    while 'columns[{0}][data]'.format(i) in args:
        fields.append(args.get('columns[{0}][data]'.format(i)))
        i += 1

    # Define table columns based on request 'data' fields
    columns = [ColumnDT(getattr(model, field), mData=field) for field in fields]
    # Define initial query and model to select columns from
    query = session.query().select_from(model)
    # Generate output based on request arguments, query and columns
    datatable = DataTables(args, query, columns)

    return jsonify(datatable.output_result())


# API Routes
@app.route('/api/v1.0/test', methods=['GET'])
def test():
    return jsonify(request.get_json())


@app.route('/api/v1.0/columns', methods=['GET'])
def get_columns():
    '''Returns column names and their types for sample and skim classes

    Note that the return value is based upon the columns defined in columns.py.
    For each column, its data type is dynamically determined from its model.
    '''

    # Get columns and column data types for each model
    result = {}
    for classname in ['DataSample', 'DataSkim', 'DataFile',
                      'MCSample', 'MCSkim', 'MCFile']:
        # Assign summary of columns to their classname
        result[classname] = models.summarize_columns(
            getattr(models, classname), {'versions', 'sample'}
        )
    return jsonify(result)


@app.route('/api/v1.0/<string:classname>/<int:id>', methods=['GET'])
@app.route('/api/v1.0/<string:classname>/<string:name>', methods=['GET'])
def get_entry(classname, id=None, name=None):
    '''Direct access to database entries through their ID or name

    This function provides direct access to any entry in the database by their
    unique ID or their unique name (if the entry is a sample).
    '''

    # Get entry
    entry = query_unique(classname, id, name)

    # Serialize and return entry
    schema = get_schema(classname)
    return jsonify(schema.dump(entry).data)


@app.route('/api/v1.0/query', methods=['GET'])
@app.route('/api/v1.0/query/latest', methods=['GET'])
@app.route('/api/v1.0/query/<int:limit>/<int:offset>', methods=['GET'])
@require_json
@excepts
def get_query(limit=None, offset=None):
    '''Powerful query interface based on JSON

    This function provides a query interface mediated by the JSON payload of
    the GET request. If the "latest" route is used, it will return the sample
    and skim with the highest ID, respectively, which satisfy the query.

    The limit and offset route can be used to chunk your query, when it is
    particularly large. Note that the limit and offset always apply to a
    combination of the provided models. For example if you have 10 samples and
    with 50 skims each and request chunks of 55, you will get the first sample
    with 50 skims and the second one with 5 in your first iteration.
    '''

    # Retrieve JSON and setup models
    json = request.get_json()
    models = [get_model(classname) for classname in json['models']]

    # Ensure that there is a relationship between the models
    relationships = [None]
    for relationship in inspect(models[0]).relationships:
        if relationship.mapper.class_ in models[1:]:
            relationships.append(relationship.__str__().split('.', 1)[1])

    # Initialize joined query with models in request JSON
    query = None
    # Apply filters for the models
    for classname, model, relation in zip(json['models'], models, relationships):
        # Either setup the query, or join and later eager load it
        if not query:
            query = session.query(model)
        else:
            query = query.join(model)

        # Apply filters specified in the request to narrow down query
        filterset = FilterSet(model, json['filters'][classname])
        query = filterset.filter_query(query)
        # Instruct the query to eagerly load the relationship
        if relation:
            query = query.options(contains_eager(relation))

    # For latest, return only a single element
    if 'latest' in request.url:
        # Only select single combinations of parent and children; An example:
        # If one requests Samples with Skims, they are queried as (Sample,
        # Skim) pairs. By requiring distinct Sample IDs, only the first pair
        # will be returned.
        query = query.distinct(models[0].id)

        # Order the query by the IDs; An example:
        # First order by Skim ID so you have the highest on top, but the
        # primary sorting is done by Sample ID so that you get the highest
        # Sample ID first.
        for model in models:
            query = query.order_by(model.id.desc())

    # Apply limit and offset for chunked query
    if limit:
        query = query.limit(limit)
    if offset:
        query = query.offset(offset)

    # Select (nested) schema and serialize entries before returning
    if 'skims' in relationships and 'Sample' in json['models'][0]:
        schema = get_schema('Nested' + json['models'][0])
    else:
        schema = get_schema(json['models'][0])
    return jsonify(schema.dump(query.all(), many=True).data)


@app.route('/api/v1.0/<string:classname>', methods=['POST'])
@require_json
@excepts
def post_entry(classname):
    # Create entry class instance from payload
    entry = validated_instance(classname)
    # Set updater
    entry.updater = request.environ.get('HTTP_X_REMOTE_USER')

    # Add entry to database, commit and return
    session.add(entry)
    session.commit()
    return jsonify({'id': entry.id})


@app.route('/api/v1.0/<string:parentclass>/<int:id>/<string:childclass>', methods=['POST'])
@app.route('/api/v1.0/<string:parentclass>/<string:name>/<string:childclass>', methods=['POST'])
@require_json
@excepts
def post_child(parentclass, childclass, id=None, name=None):
    # Query parent sample
    parent = query_unique(parentclass, id, name)

    # Load and vaildate skim
    child = validated_instance(childclass)

    if 'Sample' in parentclass and hasattr(parent, 'skims'):
        # Set user info on skim
        child.owner = request.environ.get('HTTP_X_REMOTE_USER')
        child.updater = request.environ.get('HTTP_X_REMOTE_USER')
        # Insert skim into sample
        parent.skims.append(child)
    elif 'Skim' in parentclass and hasattr(parent, 'files'):
        # Insert file into skim
        parent.files.append(child)
    else:
        raise APIException('URL is not formed properly.')

    session.commit()
    return jsonify({'id': child.id})


@app.route('/api/v1.0/<string:classname>/<int:id>', methods=['PUT'])
@app.route('/api/v1.0/<string:classname>/<string:name>', methods=['PUT'])
@require_json
@excepts
def put_entry(classname, id=None, name=None):
    # Query for entry
    entry = query_unique(classname, id, name)

    # Validate input and update entry
    schema = get_schema(classname)
    updated_entry, errors = schema.load(request.get_json(), instance=entry)
    if errors:
        session.rollback()
        raise APIException('Fields are not configured correctly.', payload=errors)

    # Set updater
    updated_entry.updater = request.environ.get('HTTP_X_REMOTE_USER')

    session.commit()
    return jsonify({})


@app.route('/api/v1.0/<string:classname>/<int:id>', methods=['DELETE'])
@app.route('/api/v1.0/<string:classname>/<string:name>', methods=['DELETE'])
@excepts
def delete_entry(classname, id=None, name=None):
    # Query for entry
    entry = query_unique(classname, id, name)

    # Delete entry and commit changes to database
    session.delete(entry)
    session.commit()
    return jsonify({})
