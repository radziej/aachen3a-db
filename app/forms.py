'''Input forms for the database's tables

The input forms defined in this file are created with WTForms.
'''

# wtforms imports
from wtforms import Field, Form, BooleanField, StringField, IntegerField
from wtforms import FloatField, validators
from wtforms.ext.dateutil.fields import DateTimeField
from wtforms.widgets import TextInput
from wtforms.validators import ValidationError

# local imports
from . import models


class Lengths(object):
    '''Checks the length of each element of a list'''

    def __init__(self, min=-1, max=-1, message=None):
        self.min = min
        self.max = max
        if not message:
            message = u'Fields must be between {0} and {1} characters long.'\
                      .format(min, max)
        self.message = message

    def __call__(self, form, field):
        if field.data:
            # Check each field for its length
            for entry in field.data:
                l = len(entry)
                # Length smaller than minimum OR longer than maximum (if set)
                if l < self.min or (self.max != -1 and l > self.max):
                    raise ValidationError(self.message)


class ListField(Field):
    '''Field of list entries, separated by commata

    Spaces between the values do no matter, as they are stripped automtacally.
    '''

    widget = TextInput()

    def _value(self):
        if self.data:
            return u', '.join(self.data)
        else:
            return u''

    def process_formdata(self, valuelist):
        if len(valuelist) == 1 and not valuelist[0]:
            self.data = []
        else:
            self.data = [x.strip() for x in valuelist[0].split(',')]


def none_if_empty(x):
    '''Filter to replace empty objects with None'''

    return x or None


class DataSampleForm(Form):
    name = StringField('Name', [validators.Length(max=150)])
    energy = IntegerField('Energy', [validators.NumberRange(min=1, max=1000)])
    # user
    updater = StringField('Updater', [validators.Length(max=20)])
    comments = StringField('Comments', [validators.Length(max=300)], [none_if_empty])


class DataSkimForm(Form):
    # user
    owner = StringField('Owner', [validators.Length(max=20)])
    updater = StringField('Updater', [validators.Length(max=20)])
    comments = StringField('Comments', [validators.Length(max=300)], [none_if_empty])
    # sample
    datasetpath = StringField('Datasetpath', [validators.Length(max=300)])
    sites = ListField('Sites', [Lengths(min=1, max=40)])
    # skimmer
    version = StringField('Version', [validators.Length(max=40)])
    cmssw = StringField('CMSSW', [validators.Length(max=40)])
    globaltag = StringField('Globaltag', [validators.Length(max=80)])
    # time and status
    created = DateTimeField('Created', [validators.Optional()],
                               display_format='%Y-%m-%d %H:%M:%S%z')
    finished = DateTimeField('Finished', [validators.Optional()],
                               display_format='%Y-%m-%d %H:%M:%S%z')
    deprecated = DateTimeField('Deprecated', [validators.Optional()],
                               display_format='%Y-%m-%d %H:%M:%S%z')
    private = ListField('Private', [Lengths(min=0, max=40)])
    # run ranges
    run_json = StringField('Run JSON', [validators.Length(max=50)])
    run_first = IntegerField('First Run', [validators.NumberRange(min=0)])
    run_last = IntegerField('Last Run', [validators.NumberRange(min=0)])
    # luminosity
    processed_json = StringField('Processed JSON', [validators.Length(max=700)])



class MCSampleForm(Form):
    name = StringField('Name', [validators.Length(max=150)])
    energy = IntegerField('Energy', [validators.NumberRange(min=1, max=1000)])
    # user
    updater = StringField('Updater', [validators.Length(max=20)])
    comments = StringField('Comments', [validators.Length(max=300)], [none_if_empty])
    # generator
    generator = StringField('Generator', [validators.Length(max=30)])
    filterefficiency = FloatField('Filter Efficiency', [validators.NumberRange(min=0., max=1.)])
    filterefficiency_ref = StringField('Filter Efficiency Reference', [validators.Length(max=300)])
    # cross section
    crosssection = FloatField('Cross Section', [validators.NumberRange(min=0., max=100000.)])
    crosssection_order = StringField('Cross Section Order', [validators.Length(max=10)])
    crosssection_ref = StringField('Cross Section Reference', [validators.Length(max=300)])
    kfactor = FloatField('k-Factor', [validators.NumberRange(min=0., max=10.)])
    kfactor_order = StringField('k-Factor Order', [validators.Length(max=10)], [none_if_empty])
    kfactor_ref = StringField('k-Factor Reference', [validators.Length(max=300)], [none_if_empty])


class MCSkimForm(Form):
    # user
    owner = StringField('Owner', [validators.Length(max=20)])
    updater = StringField('Updater', [validators.Length(max=20)])
    comments = StringField('Comments', [validators.Length(max=300)], [none_if_empty])
    # sample
    datasetpath = StringField('Datasetpath', [validators.Length(max=300)])
    sites = ListField('Sites', [Lengths(min=1, max=40)])
    # skimmer
    version = StringField('Version', [validators.Length(max=40)])
    cmssw = StringField('CMSSW', [validators.Length(max=40)])
    globaltag = StringField('Globaltag', [validators.Length(max=80)])
    # time and status
    created = DateTimeField('Created', [validators.Optional()],
                            display_format='%Y-%m-%d %H:%M:%S%z')
    finished = DateTimeField('Finished', [validators.Optional()],
                             display_format='%Y-%m-%d %H:%M:%S%z')
    deprecated = DateTimeField('Deprecated', [validators.Optional()],
                               display_format='%Y-%m-%d %H:%M:%S%z')
    private = ListField('Private', [Lengths(min=0, max=40)])
